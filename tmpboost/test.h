#pragma once
#include "stdafx.h"
#include "Client.h"



struct Test {
	
	
	void run() {
		Client c;
		std::shared_ptr<uint8_t> buf(new uint8_t[128], [](uint8_t* p) { delete[] p; });
		buf.get()[0] = 2;
		std::string nicka("player");
		buf.get()[1] = nicka.size();
		
		uint8_t id[16] = { 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16 };
		for (uint8_t i = 0; i < buf.get()[1]; ++i) {
			buf.get()[2 + i] =nicka[i];
		}
		for (int i = 0; i < 16; ++i) {
			buf.get()[2 + buf.get()[1] + i] =id[i];
		}
		std::string id_stra((char*)id, 16);
		Velocity v = { 1234.56789f,-15.56789f ,-1233.56789f };
		AngularVelocity av = { 1234.56789f,-15.56789f ,-1233.56789f };
		Rotation r = { 1234.56789f,-15.56789f ,-1233.56789f };
		Location l = { 1234.56789f,-15.56789f ,-1233.56789f };
		c.fill_buffer(buf, &v, &av, &r, &l);
		uint8_t* a = buf.get() + 1 + buf.get()[1] + 1 + 16;
		Velocity		tmp_vel = { uint8Tofloat(a) ,uint8Tofloat(a + 4),uint8Tofloat(a + 8) };
		AngularVelocity tmp_avel = { uint8Tofloat(a + 12) ,uint8Tofloat(a + 16),uint8Tofloat(a + 20) };
		Rotation		tmp_rot = { uint8Tofloat(a + 24) ,uint8Tofloat(a + 28),uint8Tofloat(a + 32) };
		Location		tmp_loc = { uint8Tofloat(a + 36) ,uint8Tofloat(a + 40),uint8Tofloat(a + 44) };
		std::string nickb((char*)(buf.get() + 2), buf.get()[1]);

		std::string id_strb((char*)(buf.get() + 1 + buf.get()[1] + 1), 16);
		assert((tmp_vel == v) && (av == tmp_avel)&&( r == tmp_rot)&&(l == tmp_loc));
		assert(nicka == nickb);
		assert(id_strb == id_stra);
		
	}
};