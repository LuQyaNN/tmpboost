
#pragma once
#include "stdafx.h"
#include "Client.h"
#include <vector>
#include <algorithm>
#include <thread>



using boost::asio::io_service;
using boost::asio::ip::tcp;



class Server {
public:
	Server(io_service* ioservice) :service(ioservice),ep(boost::asio::ip::tcp::v4(), 80), ac(*service, ep) {
	accept();
	udp_receiver();
	
service->run();

	
	}

	
private:
	std::unordered_map<std::string, Client*> clients_nick_map;
	std::unordered_map<std::string, Client*> clients_id_map;
	io_service* service;
	tcp::endpoint ep;
	tcp::acceptor ac;
	void udp_receiver() {
		std::shared_ptr<boost::asio::ip::udp::socket> sockudp(new boost::asio::ip::udp::socket(*service, boost::asio::ip::udp::endpoint( boost::asio::ip::udp::v4(),80)));
		std::shared_ptr < uint8_t > buf(new uint8_t[128], [](uint8_t* p) {delete[] p; });
		boost::asio::ip::udp::endpoint udp_ep;
		sockudp->async_receive_from(boost::asio::buffer(buf.get(), 128), udp_ep, boost::bind(&Server::recieve_datagram, this, sockudp, buf, _1, _2));
		
		
	}

	void recieve_datagram(std::shared_ptr<boost::asio::ip::udp::socket> sock, std::shared_ptr < uint8_t > buf,const boost::system::error_code& error, std::size_t length) {
	
		std::shared_ptr < uint8_t > buf1(new uint8_t[128], [](uint8_t* p) {delete[] p; });
		boost::asio::ip::udp::endpoint ep;
		sock->async_receive_from(boost::asio::buffer(buf1.get(), 128), ep, boost::bind(&Server::recieve_datagram, this, sock, buf1, _1, _2));
		//service->run();
		if (error)  return; 
		switch (buf.get()[0]) {
		case 2: {
			std::string tmp_id((char*)(buf.get() + 1), 16);
			tmp_id.insert(16, 1, '\0');
			auto iter =clients_id_map.find(tmp_id);
			if(iter != clients_id_map.end())
				iter->second->setRepInfo(buf.get() + 17);
			
			break;
		}
		}
	}

	void accept() {
		
		std::shared_ptr<tcp::socket> sock(new tcp::socket(*service));
		ac.async_accept(*sock, boost::bind(&Server::async_accept_handler,this,sock,_1));
	}

	void async_accept_handler(std::shared_ptr<tcp::socket> sock,const boost::system::error_code & err) {
	
		if (!err)new Client(service, &clients_nick_map, &clients_id_map, sock);
		std::shared_ptr<tcp::socket> sock1(new tcp::socket(*service));
		ac.async_accept(*sock1, boost::bind(&Server::async_accept_handler, this, sock1, _1));
	}

};