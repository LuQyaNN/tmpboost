
#pragma once
#include "stdafx.h"
#include <memory>
#include <string>
#include <utility>
#include <chrono>
#include <unordered_map>
#include "md5.h"
#include <utility>
#include <mutex>
#include <chrono>
#include <future>
#include <future>
#include <chrono>
//#ifdef TEST
//#include "test.h"
//#endif

struct vec3 {
	float x;
	float y;
	float z;
	bool operator==(const vec3& other) {
		return (x == other.x) && (y == other.y) && (z == other.z);
	}
	bool operator!=(const vec3& other) {
		return (x != other.x) && (y != other.y) && (z != other.z);
	}
};

typedef vec3 Velocity;
typedef vec3 AngularVelocity;
typedef vec3 Location;

struct  Rotation {
	float yaw;
	float pitch;
	float roll;
	bool operator==(const Rotation& other) {
		return (yaw == other.yaw) && (pitch == other.pitch) && (roll == other.roll);
	}
	bool operator!=(const Rotation& other) {
		return (yaw != other.yaw) && (pitch != other.pitch) && (roll != other.roll);
	}
};

struct RepInfo {
	
	Velocity vel;
	AngularVelocity avel;
	Rotation rot;
	Location loc;
	std::string Nick;
	std::string str_id;
};

//#include "vld.h"
using boost::asio::io_service;
using boost::asio::ip::tcp;



float uint8Tofloat(uint8_t* bytes) {
	return *reinterpret_cast<float*>(bytes);
}

uint8_t* floatTouint8(float* fl) {
	return reinterpret_cast<uint8_t*>(fl);
}

float Dist(const Location& l1, const Location& l2) {
	return sqrt((l2.x - l1.x)*(l2.x - l1.x) + (l2.y - l1.y)*(l2.y - l1.y) + (l2.z - l1.z)*(l2.z - l1.z));
}

static std::recursive_mutex mut;

class Client {
public:
	Client() = default;
	
	Client(io_service* ioservice,
		std::unordered_map<std::string, Client*>* cl_nick_map,
		std::unordered_map<std::string, Client*>* cl_id_map,
		std::shared_ptr<tcp::socket> sock) :
		clients_nick_map(cl_nick_map),
		clients_id_map(cl_id_map),
		service(ioservice),
		udp_ep(sock->remote_endpoint().address(), sock->remote_endpoint().port()),
		tcp_socket(sock),
		udp_socket(std::make_shared<boost::asio::ip::udp::socket>(*ioservice, boost::asio::ip::udp::v4())),
		send_clients_info_thread(std::make_shared<std::thread>(&Client::send_clients_info, this))
		{
		/*
		float t = 6.2273f;
		uint8_t* tm;// = new uint8_t[4];
		tm = floatTouint8(&t);
		std::array<uint8_t, 4> ta;
		ta[0] = tm[0]; ta[1] = tm[1]; ta[2] = tm[2]; ta[3] = tm[3];
		float t2;
		t2 = uint8Tofloat(tm);
		*/

		
		
	
		

		

		std::shared_ptr<uint8_t> buf(new uint8_t[1024], [](uint8_t* p) { delete[] p; });

		
		sock->async_read_some(boost::asio::buffer(buf.get(), 1024),
			boost::bind(&Client::recieve, this, sock, buf, _1, _2));


		std::cout << sock->remote_endpoint().address().to_string() << " connected" << std::endl;
		
		

	}
	std::string getNick()const { return Nick; }
	inline const std::array<uint8_t, 16>& getid()const { return id; };
	void setRepInfo(Velocity a, AngularVelocity b, Rotation c, Location d) {
		vel = a;
		avel = b;
		rot = c;
		loc = d;
	}

	inline void setRepInfo(uint8_t* a) {
		if (!nick_recieved)return;
		Velocity		tmp_vel = { uint8Tofloat(a) ,uint8Tofloat(a + 4),uint8Tofloat(a + 8) };
		AngularVelocity tmp_avel = { uint8Tofloat(a + 12) ,uint8Tofloat(a + 16),uint8Tofloat(a + 20) };
		Rotation		tmp_rot = { uint8Tofloat(a + 24) ,uint8Tofloat(a + 28),uint8Tofloat(a + 32) };
		Location		tmp_loc = { uint8Tofloat(a + 36) ,uint8Tofloat(a + 40),uint8Tofloat(a + 44) };
		vel = tmp_vel;
		avel = tmp_avel;
		rot = tmp_rot;
		loc = tmp_loc;
		

		//std::cout << loc.x << " " << loc.y << " " << loc.z << std::endl;
	}

	RepInfo GetRepInfo()const{
		RepInfo r ={vel,avel,rot,loc,Nick,str_id};
		return r;
	}

	const Location& getLoc()const { return loc; }

	
#ifndef TEST
	~Client() {

		send_clients_info_thread->join();
		
		clients_nick_map->erase(Nick);
		clients_id_map->erase(str_id);
		

		std::cout << "Client deleted" << std::endl;


	}
#endif
#ifdef TEST

	friend struct Test;
#endif
private:

	std::unordered_map<std::string, Client*>* clients_nick_map;
	std::unordered_map<std::string, Client*>* clients_id_map;
	
	bool nick_recieved = false;
	Velocity vel;
	AngularVelocity avel;
	Rotation rot;
	Location loc;
	std::string str_id;
	std::string Nick;
	std::array<uint8_t, 16> id;
	io_service* service;
	boost::asio::ip::udp::endpoint udp_ep;
	std::shared_ptr<tcp::socket> tcp_socket;
	std::shared_ptr<boost::asio::ip::udp::socket> udp_socket;
	std::shared_ptr<std::thread> send_clients_info_thread;
	bool prepare_for_delete = false;
	std::chrono::system_clock::time_point last_time;

	void send_clients_info() {
		for (;;) {
			Sleep(100);
			if(prepare_for_delete)return;
			std::unordered_map<std::string, RepInfo*> visible_clients;
			std::vector<RepInfo> ClientsRepInfo;
			mut.lock();
			for (auto client : *clients_id_map) {
				ClientsRepInfo.push_back(client.second->GetRepInfo());
			}
			mut.unlock();
			//mut.lock();
			for (auto& client : ClientsRepInfo) {


				if (client.str_id != str_id) {
					if (Dist(loc, client.loc) < 10000) {
						if (visible_clients.find(client.str_id) == visible_clients.end()) {
							visible_clients.insert(std::make_pair(client.str_id, &client));
						}
					} else {
						if (visible_clients.find(client.str_id) != visible_clients.end()) {
							visible_clients.erase(client.str_id);
						}
					}

				}

			}
			//mut.unlock();
			size_t buf_size=0;
			//mut.lock();
			for (auto& client : visible_clients) {
				
				buf_size+=(client.second->Nick.size() + 66);
			}
			//mut.unlock();
			if(buf_size==0)continue;
			buf_size+=2;
			std::shared_ptr<uint8_t> buf(new uint8_t[buf_size], [](uint8_t* p) { delete[] p; });
			buf.get()[0] = 2;
			buf.get()[1] = visible_clients.size();
			uint8_t* current_pos = buf.get()+2;
			//mut.lock();

			for (auto& client : visible_clients) {
				
				uint8_t nick_size = static_cast<uint8_t>(client.second->Nick.size());;
				*current_pos = nick_size;
				current_pos+=1;
				
				
				
				for (uint8_t i = 0; i <nick_size; ++i) {
					*current_pos = client.second->Nick[i];
					current_pos+=1;
				}
				for (int i = 0; i < 16; ++i) {
					*current_pos = client.second->str_id[i];
					current_pos+=1;
				}

				fill_buffer(buf,current_pos, &client.second->vel,
					&client.second->avel,
					&client.second->rot,
					&client.second->loc);

				


			}
			//mut.unlock();
			udp_socket->async_send_to(boost::asio::buffer(buf.get(), buf_size), udp_ep, [=](const boost::system::error_code& error,
				std::size_t bytes_transferred) {});
		}
	}


	void fill_buffer(std::shared_ptr<uint8_t> buf, uint8_t*& current_pos, Velocity* v, AngularVelocity* a,
		Rotation* r, Location* l) {
		uint8_t* vel_x = floatTouint8(&v->x);
		uint8_t* vel_y = floatTouint8(&v->y);
		uint8_t* vel_z = floatTouint8(&v->z);
		uint8_t* avel_x = floatTouint8(&a->x);
		uint8_t* avel_y = floatTouint8(&a->y);
		uint8_t* avel_z = floatTouint8(&a->z);
		uint8_t* rot_yaw = floatTouint8(&r->yaw);
		uint8_t* rot_pitch = floatTouint8(&r->pitch);
		uint8_t* rot_roll = floatTouint8(&r->roll);
		uint8_t* loc_x = floatTouint8(&l->x);
		uint8_t* loc_y = floatTouint8(&l->y);
		uint8_t* loc_z = floatTouint8(&l->z);
		
		add_uint8_t_to_buf( current_pos,vel_x);
		add_uint8_t_to_buf( current_pos, vel_y);
		add_uint8_t_to_buf( current_pos, vel_z);
		add_uint8_t_to_buf( current_pos, avel_x);
		add_uint8_t_to_buf( current_pos, avel_y);
		add_uint8_t_to_buf( current_pos, avel_z);
		add_uint8_t_to_buf( current_pos, rot_yaw);
		add_uint8_t_to_buf( current_pos, rot_pitch);
		add_uint8_t_to_buf( current_pos, rot_roll);
		add_uint8_t_to_buf( current_pos, loc_x);
		add_uint8_t_to_buf( current_pos, loc_y);
		add_uint8_t_to_buf( current_pos, loc_z);
		

		
	}

	void add_uint8_t_to_buf( uint8_t*& current_pos, uint8_t* float_ptr) {
		for (int i = 0; i < 4; ++i) {
			*current_pos = float_ptr[i];
			current_pos += 1;
		}
	}

	void OnRecieveNick(std::shared_ptr<uint8_t> buf, std::size_t& length) {
		nick_recieved = true;
		std::string tmpnick((char*)(buf.get() + 1), length - 2);
		if (IsValidNick(tmpnick)) {
			Nick = tmpnick;


			tmpnick.append("ballserver134");
			MD5 hash_id(tmpnick);

			id = hash_id.result();
			str_id = hash_id.result_str();
			str_id.insert(16, 1, '\0');
			mut.lock();
			clients_nick_map->insert(std::pair<std::string, Client*>(Nick, this));
			clients_id_map->insert(std::pair<std::string, Client*>(str_id, this));
			mut.unlock();
			std::shared_ptr<uint8_t> buf1(new uint8_t[17], [](uint8_t* p) { delete[] p; });
			buf1.get()[0] = 1;
			for (int i = 1; i < 17; ++i) {
				buf1.get()[i] = id[i - 1];
			}

			tcp_socket->async_send(boost::asio::buffer(buf1.get(), 17), [=](const boost::system::error_code& error,
				std::size_t bytes_transferred) {});
		} else {
			std::shared_ptr<uint8_t> buf1(new uint8_t[2], [](uint8_t* p) { delete[] p; });
			buf1.get()[0] = 0;
			buf1.get()[1] = 0;
			tcp_socket->async_send(boost::asio::buffer(buf1.get(), 1), [=](const boost::system::error_code& error,
				std::size_t bytes_transferred) {
				tcp_socket->close(); });

		}

		std::cout << Nick << std::endl;

	}

	void OnRecieveChatMsg(std::shared_ptr<uint8_t> buf, std::size_t& length) {
		std::shared_ptr<uint8_t> buf1(new uint8_t[1+length + Nick.size()], [](uint8_t* p) { delete[] p; });
		buf1.get()[0]=3;
		buf1.get()[1] = Nick.size();
		for (int i = 0; i < Nick.size(); ++i) {
			buf1.get()[i+2]= Nick[i];
		}
		for (int i = 1; i < length; ++i) {
			buf1.get()[1+Nick.size() +i]=buf.get()[i];
		}
		for (auto& client : *clients_id_map) {
			client.second->tcp_socket->async_send(boost::asio::buffer(buf1.get(), 1 + length + Nick.size()),
				[](boost::system::error_code ec, std::size_t length){});
		}
	}

	bool IsValidNick(std::string& nick) {
		return (clients_nick_map->end() == clients_nick_map->find(nick));
	}

	void recieve(std::shared_ptr<tcp::socket> sock, std::shared_ptr<uint8_t> buf,
		boost::system::error_code ec, std::size_t length) {

		if (ec) {

			std::cout << ec.message() << std::endl;

			tcp_socket->close();
			
			
			prepare_for_delete = true;
			delete this;
			return;
		} else {

			std::shared_ptr<uint8_t> buf1(new uint8_t[1024], [](uint8_t* p) { delete[] p; });
			sock->async_read_some(boost::asio::buffer(buf1.get(), 1024),
				boost::bind(&Client::recieve, this, sock, buf1, _1, _2));
		}


		switch (buf.get()[0]) {
		case 1: {//take nick
			OnRecieveNick(buf, length);
			break;
		}
		case 3://take chat message
		{
			OnRecieveChatMsg(buf, length);
			break;
		}
		}



	}
};