// tmpboost.cpp: ���������� ����� ����� ��� ����������� ����������.
//

#include "stdafx.h"
#include <iostream>
#include <memory>
#include <cstring>
#include <vector>
#include <string>
#include "Server.h"
#include "Client.h"
#ifdef TEST
#include "test.h"
#endif




using boost::asio::io_service;
using boost::asio::ip::tcp;


int main()
{

#ifndef TEST
	setlocale(LC_ALL, "");
	
	
	try {
		
		new Server(new io_service);
		
	} catch (std::exception& e) {
		std::cout << "Error " << e.what();

		std::cin.get();

	}
#else
	Test t;
	t.run();
#endif
	


    return 0;
}

