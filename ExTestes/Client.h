#pragma once
#include "boost/asio.hpp"
#include "boost/bind.hpp"
#include <string>
#include <iostream>
#include <thread>
#include <chrono>

float uint8Tofloat(uint8_t* bytes) {
	return *reinterpret_cast<float*>(bytes);
}

uint8_t* floatTouint8(float* fl) {
	return reinterpret_cast<uint8_t*>(fl);
}

struct vec3 {
	float x =0;
	float y =0;
	float z =0;
	bool operator==(const vec3& other) {
		return (x == other.x) && (y == other.y) && (z == other.z);
	}
	bool operator!=(const vec3& other) {
		return (x != other.x) && (y != other.y) && (z != other.z);
	}
};

typedef vec3 Velocity;
typedef vec3 AngularVelocity;
typedef vec3 Location;

struct  Rotation {
	float yaw =0;
	float pitch =0;
	float roll =0;
	bool operator==(const Rotation& other) {
		return (yaw == other.yaw) && (pitch == other.pitch) && (roll == other.roll);
	}
	bool operator!=(const Rotation& other) {
		return (yaw != other.yaw) && (pitch != other.pitch) && (roll != other.roll);
	}
};

class Client {
public:
	Client(boost::asio::io_service* ios_ser,
		boost::asio::ip::tcp::endpoint tep,
		boost::asio::ip::udp::endpoint uep) :ios(ios_ser), tcp_ep(tep),udp_ep(uep){
		tcp_socket = std::make_shared<boost::asio::ip::tcp::socket>(boost::asio::ip::tcp::socket(*ios_ser));
		try {
			
			tcp_socket->connect(tcp_ep);
			
			
		
	} catch (std::exception& e) {
		std::cout << "Error " << e.what();

		//std::cin.get();
	}
		boost::asio::ip::udp::endpoint up(boost::asio::ip::udp::v4(), tcp_socket->local_endpoint().port());
		udp_socket = std::make_shared<boost::asio::ip::udp::socket>(boost::asio::ip::udp::socket(*ios_ser,up));
		//udp_socket->connect(udp_ep);
		loc.z = 500;
		
		begin();
	}
private:

	
	void begin() {
		SendNick();

		std::shared_ptr<uint8_t> tcp_buf(new uint8_t[256], [](uint8_t* p) {delete[] p; });
		tcp_socket->async_receive(boost::asio::buffer(tcp_buf.get(), 256), boost::bind(&Client::recieve_tcp, this,tcp_buf, _1, _2));

		std::shared_ptr<uint8_t> udp_buf(new uint8_t[65], [](uint8_t* p) {delete[] p; });
		udp_buf.get()[0] = 2;
		fill_buffer(udp_buf, &vel, &avel, &rot, &loc);
		udp_socket->async_send(boost::asio::buffer(udp_buf.get(), 65), boost::bind(&Client::send_pos, this, _1, _2));

		std::shared_ptr<uint8_t> udp_buf1(new uint8_t[256], [](uint8_t* p) {delete[] p; });
		
		udp_socket->async_receive(boost::asio::buffer(udp_buf1.get(), 256), boost::bind(&Client::recieve_udp, this, udp_buf1, _1, _2));
	}

	void SendNick() {
		static int num = 0;
		std::string nick = "player";
		char s[10];
		
		
		srand(std::chrono::system_clock::now().time_since_epoch().count());
		nick.append(itoa(rand()%100000000,s,10));
		std::shared_ptr<char> itoa_buf(new char[10], [](char* p) {delete[] p; });
		nick.append(_itoa(++num, itoa_buf.get(), 10));
		size_t buf_size =nick.size() + 2;
		std::shared_ptr<uint8_t> buf(new uint8_t[buf_size], [](uint8_t* p) {delete[] p; });
		buf.get()[0] = 1;
		
		

		
		for (size_t i = 0; i < nick.size() + 1; ++i) {

			buf.get()[i + 1] = nick[i];
		}
		
		tcp_socket->async_send(boost::asio::buffer(buf.get(), buf_size), [=](const boost::system::error_code& error,
			std::size_t bytes_transferred) { });
		
		
	}

	void send_pos(const boost::system::error_code& error, std::size_t length) {
		
		Sleep(100);
		static unsigned int num = 0;
		srand(++num);
		

		std::shared_ptr<uint8_t> udp_buf(new uint8_t[65], [](uint8_t* p) {delete[] p; });
		if (id_str.size() != 0) {
			loc.x = rand() % 7000;
			loc.y = rand() % 7000;
			loc.z = rand() % 7000;
			udp_buf.get()[0] = 2;
			for (int i = 0; i < 16; ++i) {
				udp_buf.get()[1 + i] = id_str[i];
			}
			fill_buffer(udp_buf, &vel, &avel, &rot, &loc);
		}
		udp_socket->async_send_to(boost::asio::buffer(udp_buf.get(), 65),udp_ep, boost::bind(&Client::send_pos, this, _1, _2));
		
		
	}

	void recieve_udp(std::shared_ptr<uint8_t> buf, const boost::system::error_code& error, std::size_t length) {
		
		std::shared_ptr<uint8_t> udp_buf1(new uint8_t[256], [](uint8_t* p) {delete[] p; });
		udp_socket->async_receive(boost::asio::buffer(udp_buf1.get(), 256), boost::bind(&Client::recieve_udp, this, udp_buf1, _1, _2));
		
		
	}

	void recieve_tcp(std::shared_ptr<uint8_t> buf, const boost::system::error_code& error, std::size_t length) {
		
		
		switch (buf.get()[0]) {
		case 1: {
			for (int i = 0; i < 16; ++i) {
				id_str.append(1,(char)buf.get()[1 + i]);
			}
			
			break;
		}
		}
		std::shared_ptr<uint8_t> tcp_buf(new uint8_t[256], [](uint8_t* p) {delete[] p; });
		tcp_socket->async_receive(boost::asio::buffer(tcp_buf.get(), 256), boost::bind(&Client::recieve_tcp, this, tcp_buf, _1, _2));
	}

	void fill_buffer(std::shared_ptr<uint8_t> buf, Velocity* v, AngularVelocity* a,
		Rotation* r, Location* l) {
		uint8_t* vel_x = floatTouint8(&v->x);
		uint8_t* vel_y = floatTouint8(&v->y);
		uint8_t* vel_z = floatTouint8(&v->z);
		uint8_t* avel_x = floatTouint8(&a->x);
		uint8_t* avel_y = floatTouint8(&a->y);
		uint8_t* avel_z = floatTouint8(&a->z);
		uint8_t* rot_yaw = floatTouint8(&r->yaw);
		uint8_t* rot_pitch = floatTouint8(&r->pitch);
		uint8_t* rot_roll = floatTouint8(&r->roll);
		uint8_t* loc_x = floatTouint8(&l->x);
		uint8_t* loc_y = floatTouint8(&l->y);
		uint8_t* loc_z = floatTouint8(&l->z);
		
		for (int i = 0; i < 4; ++i) {
			buf.get()[17 + i] = vel_x[i];
			buf.get()[21 + i] = vel_y[i];
			buf.get()[25 + i] = vel_z[i];
			buf.get()[29 + i] = avel_x[i];
			buf.get()[33 + i] = avel_y[i];
			buf.get()[37 + i] = avel_z[i];
			buf.get()[41 + i] = rot_yaw[i];
			buf.get()[45 + i] = rot_pitch[i];
			buf.get()[49 + i] = rot_roll[i];
			buf.get()[53 + i] = loc_x[i];
			buf.get()[57 + i] = loc_y[i];
			buf.get()[61 + i] = loc_z[i];

		}
	}

	boost::asio::ip::tcp::endpoint tcp_ep;
	boost::asio::ip::udp::endpoint udp_ep;
	Velocity vel;
	AngularVelocity avel;
	Rotation rot;
	Location loc;
	std::string nick;
	std::string id_str;
	std::shared_ptr<boost::asio::ip::tcp::socket> tcp_socket;
	std::shared_ptr<boost::asio::ip::udp::socket> udp_socket;
	boost::asio::io_service* ios;
};
